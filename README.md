![stability-workinprogress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)

# RATIONALE #

* A guide of _good practices_ in the fulfilment of webinars and virtual meetings.
* So, it is not dogmatic, instead it will _adapt_ to the technological environment that surround both parties.
* This repo is a living document that will grow and adapt over time
![adobe_connect_entrance.jpg](https://bitbucket.org/repo/akzAxL4/images/4238431507-adobe_connect_entrance.jpg)

### What is this repository for? ###

* Quick summary
    - Procedures and checklist in events organized by the institution
* Version 1.01

### How do I get set up? ###

* Summary of set up
    - _Vide_ [Guidelines.md](https://bitbucket.org/imhicihu/streaming/src/a497dd382457c1b4962e0fd0c53726979545d76e/Guidelines.md)
    - _Vide_ [Streams_guidelines.md](https://bitbucket.org/imhicihu/streaming/src/10053b13d5c242614d0417118a7274ef769cc230/Streams_guidelines.md?fileviewer=file-view-default)
    
### Repositories related ###

* [Conferences](https://bitbucket.org/imhicihu/conferences/src/master/)
* [QR Code](https://bitbucket.org/imhicihu/qr-code/src/master/)

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/streaming/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/streaming/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/streaming/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/streaming/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/streaming/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)